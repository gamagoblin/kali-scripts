#! /bin/bash

function main() {
  SELECTIONS=$(whiptail --title "Kali Linux 2.0 Post Installation" --checklist --separate-output\
  "Select post intallation tasks to perform" 15 60 8 \
  "1" "Change root password" OFF \
  "2" "Change SSH Keys" OFF \
  "3" "Install Atom Text Editor" OFF \
  "4" "Install Starred Atom Plugins" OFF\
  "5" "Install Oh My Zsh" OFF\
  "6" "Install Powerlevel9k Theme for Zsh" OFF\
  "7" "Install Powerline Fonts" OFF\
  "8" "Install BTGuard OpenVPN Configuration Files" OFF 3>&1 1>&2 2>&3)

  exitstatus=$?
  read -a arr <<<$SELECTIONS

  if [ $exitstatus = 0 ]; then
    clear
    echo ''
      for i in "${arr[@]}"
        do
          if [ "$i" = "1" ]; then
            changerootpassword
          elif [ "$i" = "2" ]; then
            changesshkeys
          elif [ "$i" = "3" ]; then
            installatom
          elif [ "$i" = "4" ]; then
            installatomstars
          elif [ "$i" = "5" ]; then
            installohmyzsh
          elif [ "$i" = "6" ]; then
            installpowerlevel9k
          elif [ "$i" = "7" ] ; then
            installpowerlinefonts
          elif [ "$i" = "8" ] ; then
            installbtguardconfigs
          fi
        done
  else
      echo "Cancelled operation."
  fi

}

show_info() {
  echo -e "\033[1;34m$@\033[0m"
}

show_success() {
  echo -e "\033[1;32m$@\033[0m"
}

show_error() {
  echo -e "\033[1;31m$@\033[m" 1>&2
}

function changerootpassword() {
    show_info "Change Root Password"
    echo ''
    passwd
}

function changesshkeys() {
  show_info "Changing SSH Keys"
  echo ''
  pushd /etc/ssh
  mkdir default_kali_keys
  mv ssh_host_* default_kali_keys
  dpkg-reconfigure openssh-server
  popd
}

function installatom() {
  show_info "Install Atom Text Editor"
  echo ''
  pushd /tmp
  mkdir atom-install
  cd atom-install
  wget https://github.com/atom/atom/releases/download/v1.6.2/atom-amd64.deb
  dpkg -i atom-amd64.deb
  cd ..
  rm -rf atom-install
  popd
}

function installatomstars() {
  show_info "Installing Stared Atom Plugins"
  echo ''
  apm stars --install
}

function installohmyzsh() {
  show_info "Installing Oh My Zsh"
  echo ''
  apt-get install zsh
  sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
}

function installpowerlevel9k() {
  show_info "Installing Powerlevel9k theme for Zsh"
  echo ''
  cd ~/.oh-my-zsh/custom
  git clone https://github.com/bhilburn/powerlevel9k.git themes/powerlevel9k
  sed -i '/ZSH_THEME="robbyrussell"/ c\ZSH_THEME="powerlevel9k/powerlevel9k"' ~/.zshrc
  source ~/.zshrc
}

function installpowerlinefonts() {
  show_info "Installing Powerline fonts"
  echo ''
  pushd /tmp
  git clone https://github.com/powerline/fonts.git
  cd fonts
  ./install.sh
  cd ..
  rm -rf fonts
  popd
  show_success "Powerline fonts installed"
  show_error  "TODO: Go into your terminal preferences and change your font to Powerline"
}

function installbtguardconfigs() {
  show_info "Installing BTGuard OpenVPN Configuration Files"
  echo ''
  apt-get install network-manager-openvpn-gnome -y
  wget -O /etc/openvpn/btguard.ca.crt http://btguard.com/btguard.ca.crt
  wget -O /etc/openvpn/btguard.conf http://btguard.com/btguard.conf
}

main
